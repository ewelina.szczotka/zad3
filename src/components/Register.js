import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import { signupUser } from "../actions/authActions";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";

class Register extends Component {
  state = {
    email: "",
    password: "",
    confirmPassword: "",
    errors: {}
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    const newUser = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.signupUser(newUser, this.props.history);
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="flex justify-center register">
        <Form onSubmit={this.onSubmit}>
          <Form.Group controlId="formGridEmail">
            <Form.Label>Email:</Form.Label>
            <Form.Control
              type="email"
              name="email"
              placeholder="Enter email"
              onChange={this.onChange}
              value={this.state.email}
              error={errors.email}
            />
            <Form.Text className="text-muted">{errors.email}</Form.Text>
          </Form.Group>
          <Form.Group controlId="formGridPassword">
            <Form.Label>Password:</Form.Label>
            <Form.Control
              type="password"
              name="password"
              placeholder="Enter password"
              onChange={this.onChange}
              value={this.state.password}
              error={errors.password}
            />
          </Form.Group>
          <Form.Text className="text-muted">{errors.password}</Form.Text>
          <Form.Group controlId="formGridConfirmPassword">
            <Form.Label>Confirm password:</Form.Label>
            <Form.Control
              type="password"
              name="confirmPassword"
              placeholder="Confirm password"
              onChange={this.onChange}
              value={this.state.confirmPassword}
              error={errors.confirmPassword}
            />
          </Form.Group>
          <Form.Text className="text-muted">{errors.confirmPassword}</Form.Text>
          <Button block type="submit" variant="secondary">
            Submit
          </Button>
        </Form>
      </div>
    );
  }
}

Register.propTypes = {
  signupUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { signupUser }
)(withRouter(Register));
