import React from "react";
import { shallow } from "../enzyme";
import TodoPanel from "./TodoPanel";
import TodoList from "./TodoList";
import AddTodo from "./AddTodo";

describe("TodoPanel", () => {
  it("should render component", () => {
    const wrapper = shallow(<TodoPanel />);
  });
});

describe("TodoPanel", () => {
  it("should have 0 todos", () => {
    const wrapper = shallow(<TodoPanel />);
    expect(wrapper.state("todos")).toHaveLength(0);
  });
});
