import React, { Component } from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faCheck } from "@fortawesome/free-solid-svg-icons";

class Todo extends Component {
  render() {
    const { _id, name } = this.props.todo;
    return (
      <div className="todo">
        <p
          style={{
            textDecoration: this.props.todo.completed ? "line-through" : "none"
          }}
        >
          <button onClick={() => this.props.handleChecked(_id)}>
            <FontAwesomeIcon icon={faCheck} />
          </button>
          {name}
          <button
            onClick={() => this.props.handleDelete(_id)}
            style={{ float: "right" }}
          >
            <FontAwesomeIcon icon={faTimes} />
          </button>
        </p>
      </div>
    );
  }
}

Todo.propTypes = {
  todo: PropTypes.object.isRequired,
  handleChecked: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired
};

export default Todo;
