import React, { Component } from "react";

class Title extends Component {
  render() {
    return (
      <div>
        <h1 style={{ margin: "1em" }}>Todos</h1>
      </div>
    );
  }
}

export default Title;
