import React from "react";
import { shallow } from "../enzyme";

import TodoList from "./TodoList";
import Todo from "./Todo";

const todos = [
  { id: 1, name: "abc", completed: false },
  { id: 2, name: "abc", completed: false }
];

describe("TodoList", () => {
  it("should render component", () => {
    const wrapper = shallow(<TodoList todos={todos} />);
  });
});

describe("List tests", () => {
  it("renders todos correctly", () => {
    const wrapper = shallow(<TodoList todos={todos} />);
    expect(wrapper.find(Todo)).toBeDefined();
    expect(wrapper.find(Todo)).toHaveLength(todos.length);
  });
});
