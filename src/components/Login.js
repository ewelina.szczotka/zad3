import React, { Component } from "react";
import {
  Button,
  FormGroup,
  FormControl,
  FormLabel,
  Form
} from "react-bootstrap";
import { loginUser } from "../actions/authActions";
import { connect } from "react-redux";
import PropTypes from "prop-types";

class Login extends Component {
  state = {
    email: "",
    password: "",
    errors: {}
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/");
    }
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  onChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.loginUser(userData);
  };

  render() {
    const { errors } = this.state;
    return (
      <div className="login">
        <Form onSubmit={this.onSubmit}>
          <FormGroup controlId="email">
            <FormLabel>Email:</FormLabel>
            <FormControl
              autoFocus
              placeholder="Enter email"
              type="email"
              name="email"
              onChange={this.onChange}
              value={this.state.email}
              error={errors.email}
            />
            <Form.Text className="text-muted">
              {errors.email}
              {errors.emailnotfound}
            </Form.Text>
          </FormGroup>
          <FormGroup controlId="password">
            <FormLabel>Password:</FormLabel>
            <FormControl
              onChange={this.onChange}
              value={this.state.password}
              error={errors.password}
              placeholder="Password"
              type="password"
              name="password"
            />
            <Form.Text className="text-muted">
              {" "}
              {errors.password}
              {errors.passwordincorrect}
            </Form.Text>
          </FormGroup>
          <Button block variant="secondary" type="submit">
            Login
          </Button>
        </Form>
      </div>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};
const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});
export default connect(
  mapStateToProps,
  { loginUser }
)(Login);
