import React, { Component } from "react";
import { Nav, Navbar } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../actions/authActions";

class NavigationBar extends Component {
  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  render() {
    const isLoggedIn = this.props.auth.isAuthenticated;
    return (
      <Navbar bg="light" expand="lg">
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="justify-content-center">
            <Nav.Link exact eventKey="1" as={NavLink} to="/">
              PANEL
            </Nav.Link>
            {isLoggedIn ? (
              <React.Fragment>
                <button
                  style={{ backgroundColor: "transparent", border: "none" }}
                  onClick={this.onLogoutClick}
                >
                  LOGOUT
                </button>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <Nav.Link eventKey="2" as={NavLink} to="/login">
                  LOGIN
                </Nav.Link>
                <Nav.Link eventKey="3" as={NavLink} to="/register">
                  REGISTER
                </Nav.Link>
              </React.Fragment>
            )}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

NavigationBar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(NavigationBar);
