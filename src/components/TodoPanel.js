import React, { Component } from "react";
import shortid from "shortid";
import AddTodo from "./AddTodo";
import TodoList from "./TodoList";
import Title from "./Title";
import axios from "axios";

class TodoPanel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: [],
      newTodo: "",
      nameError: ""
    };
  }

  componentDidMount() {
    axios
      .get("/todos")
      .then(response => {
        this.setState({ todos: response.data });
        console.log(response.data);
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  handleInput = e => {
    this.setState({
      newTodo: e.target.value,
      nameError: ""
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    if (this.state.newTodo === "") {
      this.setState({
        nameError: "Add thing to do."
      });
    } else {
      const todo = {
        _id: shortid.generate(),
        name: this.state.newTodo,
        completed: false
      };
      axios
        .post("/todos", todo)
        .then(res => {
          this.setState({
            todos: [...this.state.todos, todo]
          });
        })
        .catch(err => console.log(err));
    }
  };

  handleDelete = id => {
    axios
      .delete(`/todos/${id}`)
      .then(res => {
        this.setState(previousState => {
          return {
            todos: previousState.todos.filter(t => t._id !== id)
          };
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleChecked = id => {
    const todo = this.state.todos.find(t => t._id === id);
    todo.completed = !todo.completed;
    axios.put(`/todos/${id}`, todo).then(res => {
      this.setState({
        todos: this.state.todos.map(todo => {
          return todo;
        })
      });
    });
  };

  render() {
    return (
      <div className="flex justify-center wrapper">
        <Title />
        <AddTodo
          handleInput={this.handleInput}
          handleSubmit={this.handleSubmit}
          newTodo={this.newTodo}
        />
        <p className="text-muted" style={{ marginTop: "10px" }}>
          {this.state.nameError}
        </p>
        {!this.state.todos.length ? <h3>Nothing to do!</h3> : null}
        <TodoList
          todos={this.state.todos}
          handleDelete={this.handleDelete}
          handleChecked={this.handleChecked}
        />
      </div>
    );
  }
}

export default TodoPanel;
