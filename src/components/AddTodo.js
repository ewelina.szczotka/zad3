import React from "react";

const AddTodo = props => {
  return (
    <form className="todo-input" onSubmit={props.handleSubmit}>
      <input
        type="text"
        placeholder="Add todo"
        onChange={props.handleInput}
        value={props.newTodo}
      />
      <button type="submit" name="submit" value="submit">
        ADD
      </button>
    </form>
  );
};

export default AddTodo;
