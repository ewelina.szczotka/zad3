import React, { Component } from "react";
import Todo from "./Todo";
import PropTypes from "prop-types";

class List extends Component {
  render() {
    return (
      <div className="list">
        {this.props.todos.map((todo, key) => (
          <Todo
            key={todo._id}
            todo={todo}
            handleDelete={this.props.handleDelete}
            handleChecked={this.props.handleChecked}
          />
        ))}
      </div>
    );
  }
}

List.propTypes = {
  todos: PropTypes.array.isRequired,
  handleChecked: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired
};

export default List;
