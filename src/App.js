import React, { Component } from "react";
import { Route, BrowserRouter } from "react-router-dom";
import TodoPanel from "./components/TodoPanel";
import Register from "./components/Register";
import Login from "./components/Login";
import NavigationBar from "./components/NavigationBar";
import { Provider } from "react-redux";
import store from "./store";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./actions/authActions";

if (localStorage.jwtToken) {
  const token = localStorage.jwtToken;
  setAuthToken(token);
  const decoded = jwt_decode(token);
  store.dispatch(setCurrentUser(decoded));
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    store.dispatch(logoutUser());
    window.location.href = "./login";
  }
}

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Provider store={store}>
          <BrowserRouter>
            <NavigationBar auth={this.props.auth} />
            <Route exact path="/" component={TodoPanel} />
            <Route path="/register" component={Register} />
            <Route path="/login" component={Login} />
          </BrowserRouter>
        </Provider>
      </React.Fragment>
    );
  }
}

export default App;
